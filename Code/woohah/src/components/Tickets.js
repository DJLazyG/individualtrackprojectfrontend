import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TicketItem from './models/TicketItem'

export class Tickets extends Component {
    render(){
        return this.props.tickets.map((ticket)=>(
                <TicketItem key={ticket.id} ticket={ticket} buyTicket={this.props.buyTicket}/>
            ));
    }
}



Tickets.propTypes = {
    tickets:PropTypes.array.isRequired,
    buyTicket:PropTypes.func.isRequired
}
export default Tickets