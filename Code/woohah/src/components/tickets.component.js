import React, { Component } from "react";
import authHeader from '../services/auth-header';
import Tickets from "./Tickets"
import UserService from "../services/user.service";
import axios from 'axios';

export default class TicketsComponent extends Component{
    constructor(props){
        super(props);

        this.state={
            content:[]
        };
    }

    componentDidMount(){
        UserService.getTickets().then(
            response => {
                this.setState({
                    content:response.data
                });
            }//,
            // error => {
            //     this.setState({
            //         content:
            //         [(error.response && error.response.data) ||
            //             error.message ||
            //             error.toString()]
                    
            //     });
            //}
        );
    }

    // buyTicket(e){
    //     // this.props.ticket.map(this.buyTicket,this);
    //     const user = JSON.parse(localStorage.getItem('user'));
    //     console.log('user id='+user.id);
    //     console.log(e);
    //     // e.preventDefault();
    //     userService.buyTicket(user.id,e);
    // }

    buyTicket=(ticketId)=>{
        const user = JSON.parse(localStorage.getItem('user'));
        
        console.log(user);
        return axios.post(`http://localhost:8080/api/tickets/${user.id}`,ticketId,{headers:authHeader()});
    }

    render(){
        return(
            <div className="ticket-container">
                
                    <Tickets tickets={this.state.content} buyTicket={this.buyTicket}/>
                
            </div>
        );
    }
}