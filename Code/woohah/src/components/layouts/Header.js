import React from 'react'
import {Link} from 'react-router-dom'

function Header() {
    return (
        <header style={headerStyle}>
            <div>
                <img style = {logoStyle} src = "./pictures/WooHahLogo.png" alt = "The Woo HaH logo"></img>
            </div>
            <div>
                <Link style={linkStyle} to="/">Home</Link>|
                <Link style={linkStyle} to="/about">About</Link>
            </div>
        </header>
    )
}
const headerStyle = {
    background: '#cc33ff',
    color: '#ffffff',
    textAlign:'center',
    padding:'10px'
}

const linkStyle={
    color: '#262626',
    textDecoration:'none'
}
const logoStyle={
    height: '7.5rem',
    width: '7.5rem'
}

export default Header;

