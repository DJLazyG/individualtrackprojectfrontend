import { Component } from "react";
import PropTypes from 'prop-types';
import '../../App.css';

export class EventItem extends Component{

    getStyle=()=>{
        return{
            background:"#f4f4f4",
            padding:'15px',
            borderBottom:'1px #ccc dotted',
            display:'flex'
        }
    }


    render(){
        const{id, name, description, pictureName}=this.props.event;
        
        return(
            
            <div className="eventBox">
                <div className="eventNameBox">{name}</div>
                <div className="eventDescriptionBox">{description}</div>
                <div className="eventPictureBox">
                    
                    <img className="eventImage" src={`./pictures/${pictureName}`} alt = {pictureName}></img>
                </div>
                    
                
            </div>
        )
    }
}


EventItem.propTypes = {
    event:PropTypes.object.isRequired
}
export default EventItem;