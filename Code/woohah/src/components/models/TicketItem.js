import { Component } from "react";
import PropTypes from 'prop-types';
import '../../App.css';
import userService from "../../services/user.service";
import Form from "react-validation/build/form";

export class TicketItem extends Component{

    

    getStyle=()=>{
        return{
            background:"#f4f4f4",
            padding:'15px',
            borderBottom:'1px #ccc dotted',
            display:'flex'
        }
    }

    render(){
        const{id, name, description, price}=this.props.ticket;
        
        return(
            
            <div className="ticketBox">
                <div className="ticketNameBox">{name}</div>
                <div className="ticketDescriptionBox">{description}</div>
                <div className="ticketPriceBox">{price} Euro</div>
                <button className="buyButton" onClick={this.props.buyTicket.bind(this, id)}>Buy!</button>
                {/* <div className="eventPictureBox">
                    <img className="eventImage" src={`./pictures/${pictureName}`} alt = {pictureName}></img>
                </div> */}
                    
                
            </div>
        )
    }
}


TicketItem.propTypes = {
    ticket:PropTypes.object.isRequired,
    buyTicket:PropTypes.func.isRequired
}
export default TicketItem;