import { Component } from "react";
import PropTypes from 'prop-types';


export class PersonItem extends Component{

    getStyle=()=>{
        return{
            background:"#f4f4f4",
            padding:'10px',
            borderBottom:'1px #ccc dotted'
        }
    }


    render(){
        const{id, username, password, email,roles,tickets}=this.props.person;
        
        return(
            
            <div className= "personBox">
                <p>
                    {id} // {username} // {email} // {roles.map(role => <span>{role.name} </span>)}//{tickets.map(ticket=><span>{ticket.name}</span>)}
                    <button onClick={this.props.deletePerson.bind(this,id)} style={btnDeleteStyle}>Delete</button>
                    <button onClick={this.props.updatePerson.bind(this,id)} style={btnUpdateStyle}>Update</button>
                </p>
                
            </div>
        )
    }
}
const btnDeleteStyle={
    background: 'red',
    color:'white',
    borderRadius:'50%',
    cursor:'pointer',
    float:'right'
}
const btnUpdateStyle={
    background: 'yellow',
    color:'black',
    borderRadius:'50%',
    cursor:'pointer',
    float:'right'
}

PersonItem.propTypes = {
    person:PropTypes.object.isRequired,
    updatePerson:PropTypes.func.isRequired,
    deletePerson:PropTypes.func.isRequired
}
export default PersonItem;