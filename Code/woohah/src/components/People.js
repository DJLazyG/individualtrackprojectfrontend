
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PersonItem from './models/PersonItem'

export class People extends Component {
    render(){
        return this.props.people.map((person)=>(
                <PersonItem key={person.id} person={person} 
                updatePerson={this.props.updatePerson} deletePerson={this.props.deletePerson}
                />
            ));
    }
}



People.propTypes = {
    people:PropTypes.array.isRequired,
    updatePerson:PropTypes.func.isRequired,
    deletePerson:PropTypes.func.isRequired
}
export default People
