import React, { Component } from "react";
import axios from 'axios';
import UserService from "../services/user.service";
import People from "./People";
import authHeader from '../services/auth-header';

export default class BoardAdmin extends Component{
    constructor(props){
        super(props);

        this.state = {
            people:[]
        };
    }

    componentDidMount(){
        UserService.getAdminBoard().then(
            response=>{
                this.setState({
                    people:response.data
                });
            }//,
            // error => {
            //     console.log(error);
                // this.setState({
                //     content:
                //     (error.response &&
                //         error.response.data &&
                //         error.response.data.message) ||
                //     error.message ||
                //     error.toString()
                // });
            //}
        );
    }
    
    deletePerson = (id) =>{
        axios.delete(`http://localhost:8080/api/users/${id}`,{headers:authHeader()})
        .then(res=>this.setState({people: [...this.state.people.filter(person => person.id !== id)]}));    
    }
    
    // isAdmin=["ROLE_USER","ROLE_MODERATOR"]
    
    updatePerson = (id)=>{
        axios.put(`http://localhost:8080/api/users/${id}`,{headers:authHeader()})
        .then(res=>this.setState({people: [...this.state.people.fill(person=>person.id===id)]}))
    }

    render() {
        return(
            <div className="container">
                <header className="jumbotron bg-pink">
                    <People  people={this.state.people} updatePerson={this.updatePerson} deletePerson={this.deletePerson}/>
                    {/* <h3>{this.state.content}</h3> */}
                </header>
            </div>
        );
    }
}