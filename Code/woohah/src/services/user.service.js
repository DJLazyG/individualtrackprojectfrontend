import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8080/api/test/';

class UserService{
    getPublicContent(){
        return axios.get('http://localhost:8080/api/event',{headers:authHeader()});
    }
    getTickets(){
        return axios.get('http://localhost:8080/api/tickets',{headers:authHeader()});
    }


    getUserBoard(){
        return axios.get(API_URL+'user',{headers:authHeader()});
    }

    getModeratorBoard(){
        return axios.get(API_URL+'mod',{headers:authHeader()});
    }

    getAdminBoard(){
        return axios.get('http://localhost:8080/api/users/all',{headers:authHeader()});
        //return axios.get(API_URL+'admin',{headers:authHeader()});
    }
}

export default new UserService();