import axios from 'axios';
import authHeader from './auth-header';

class AdminService{
    deletePerson = (id) =>{
        return axios.delete(`http://localhost:8080/api/users/${id}`,{headers:authHeader()})
        .then(res=>this.setState({people: [...this.state.people.filter(person => person.id !== id)]}));    
      }
      
    updatePerson = (id, name, password, email, isAdmin)=>{
        axios.put(`http://localhost:8080/api/users/${id}`,{headers:authHeader()})
        .then(res=>this.setState({people: [...this.state.people.fill(person=>person.id===id)]}))
      }
}

export default AdminService;